<?php


namespace App\Services\Admin\CRM\Order;

use App\Dto\Admin\CRM\Order\OrderFilterDto;
use App\Exceptions\NoDataForExportException;
use App\Exports\CRM\Order\OrderExport;
use App\Http\Resources\Admin\CRM\Order\OrderResource;
use App\Models\CRM\Order\Order;
use Carbon\Carbon;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\URL;
use Maatwebsite\Excel\Facades\Excel;

/**
 * Class OrderService
 * @package App\Services\CRM\Order
 */
class OrderService
{
    /**
     * @var string[]
     */
    public $checkedFieldsListWithTranslations = [
        'order_id' => 'Id заказа',
        'parcel_id' => 'Id посылки',
        'full_name' => 'ФИО',
        'locality' => 'Город',
        'address' => 'Адрес',
        'region' => 'Регион',
        'promo_code' => 'Промокод',
        'comment' => 'Комментарий',
        'phone' => 'Телефон',
        'email' => 'Почта',
        'created_at' => 'Дата создания',
        'updated_at' => 'Дата обновления',
        'delivery' => 'Способ доставки',
        'delivery_date' => 'Дата доставки',
        'payment' => 'Способ оплаты',
        'parcel_delivery_status' => 'Статус доставки посылки',
        'payment_status' => 'Статус оплаты',
        'amount' => 'Общая сумма',
        'cost_of_delivery' => 'Стоимость доставки',
        'departure_date' => 'Дата доставки',
        'product_sku' => 'SKU', // кликабельное поле. ссылка на витрину
        'product_price' => 'Стоимость товара',
        'product_discount' => 'Скидка',
        'product_merchant' => 'Имя продавца (Юридическое название)',
        'product_payment_status' => 'Статус оплаты',
        'product_return_order_status' => 'Статус возврата посылки',
        'product_delivery_status' => 'статус доставки посылки',
        'product_status_id' => 'Статус товара',
        'zip_code' => 'Индекс'
    ];

    public $checkedFieldsList = [
        'order_id',
        'parcel_id',
        'full_name',
        'locality',
        'address',
        'region',
        'promo_code',
        'comment',
        'phone',
        'email',
        'created_at',
        'updated_at',
        'delivery',
        'delivery_date',
        'payment',
        'parcel_delivery_status',
        'payment_status',
        'amount',
        'cost_of_delivery',
        'departure_date',
        'product_sku',
        'product_price',
        'product_discount',
        'product_merchant',
        'product_payment_status',
        'product_return_order_status',
        'product_delivery_status',
        'product_status_id',
        'zip_code'
    ];

    /**
     * @var string[]
     */
    public $productFieldsForExport = [
        'product_sku',
        'product_price',
        'product_discount',
        'product_merchant',
        'product_payment_status',
        'product_return_order_status',
        'product_delivery_status',
        'product_status_id'
    ];

    /**
     * @var string[]
     */
    public $listFilters = [
        'start_date',
        'end_date',
        'full_name',
        'locality',
        'region',
        'address',
        'promo_code',
        'comment',
        'phone',
        'email',
        'order_id',
        'delivery_id',
        'departure_date',
        'payment_id',
        'payment_status_id',
        'amount',
        'cost_of_delivery',
        'delivery_date',
        'zip_code'
    ];


    /**
     * @param OrderFilterDto $orderFilterDto
     * @return LengthAwarePaginator
     */
    public function getOrdersWithFilters(OrderFilterDto $orderFilterDto): LengthAwarePaginator
    {
        return Order::with([
            'parcels.products',
            'parcels.deliveryStatus',
            'parcels.delivery',
            'promoCode',
            'deliveryStatus',
            'delivery',
            'payment',
            'merchant',
            'paymentStatus'
        ])
            ->filter($orderFilterDto)
            ->paginate(config('paginate.admin.orders'));
    }

    /**
     * @param $orders
     * @return AnonymousResourceCollection
     */
    public function getOrdersWithFiltersResourceCollection($orders): AnonymousResourceCollection
    {
        return OrderResource::collection($orders);
    }


    /**
     * @param int $id
     * @return OrderResource
     */
    public function getOrderResource(int $id)
    {
        return new OrderResource(Order::with(['parcels'])
            ->findOrFail($id));
    }

    /**
     * @param OrderFilterDto $orderFilterDto
     * @param array $checkedFields
     * @return array
     */
    public function getDataForOrdersExport(OrderFilterDto $orderFilterDto): array
    {
        $orders = Order::filter($orderFilterDto)
            ->with([
                'parcels.deliveryStatus',
                'parcels.delivery',
                'parcels.products',
                'delivery',
                'payment',
                'merchant',
                'paymentStatus'
            ])->get();

        $data = [];

        $orders->each(function ($order) use (&$data, $orderFilterDto)  {
            $countProducts = optional($order->parcels->first())->products
                ? count(optional($order->parcels->first())->products)
                : 0;

            $parcel_delivery_status = optional($order->parcels, function ($parcels) {
                return optional($parcels->first(), function ($parcel) {
                    return optional($parcel->deliveryStatus, function ($deliveryStatus) {
                        return optional($deliveryStatus)->name;
                    });
                });
            });

            $discount = optional($order->promoCode, function ($promoCode) {
                return optional($promoCode->discount, function ($discount) {
                    return optional($discount)->percent;
                });
            });

            $merchant = optional($order->merchant, function ($merchant) {
                return $merchant->name;
            });

            $paymentStatus = optional($order->paymentStatus, function ($paymentStatus) {
                return $paymentStatus->name;
            });

            $product_return_order_status = 'product_return_order_status';

            $dataProduct = [
                'parcel_delivery_status' => $parcel_delivery_status,
                'discount' => $discount,
                'merchant' => $merchant,
                'payment_status' => $paymentStatus,
                'product_return_order_status' => $product_return_order_status
            ];

            if ($this->checkForThePresenceOfAValueInAnArray($this->getProductFieldsForExport(), $orderFilterDto->checkedFields)) {

                for ($i = 0; $i < $countProducts; $i++) {
                    $data[] = [
                        'order_id' => $order->id,
                        'parcel_id' => optional(optional($order->parcels)->first())->id,
                        'full_name' => $order->full_name,
                        'locality' => $order->locality,
                        'address' => $order->address,
                        'region' => $order->region,
                        'promo_code' => optional($order->promoCode)->code,
                        'merchant' => optional($order->merchant)->name,
                        'comment' => $order->comment,
                        'phone' => $order->phone,
                        'email' => $order->email,
                        'created_at' => $order->created_at,
                        'updated_at' => $order->updated_at,
                        'delivery' => optional($order->delivery)->name,
                        'delivery_date' => $order->delivery_date,
                        'payment' => optional($order->payment)->name,
                        'parcel_delivery_status' => $parcel_delivery_status,
                        'payment_status' => optional($order->paymentStatus)->name,
                        'amount' => $order->amount,
                        'cost_of_delivery' => $order->cost_of_delivery,
                        'departure_date' => $order->departure_date,
                        'product' => optional($order->parcels, function ($parcels) use ($i, $dataProduct) {
                            return optional($parcels->first(), function ($parcel) use ($i, $dataProduct) {
                                return optional($parcel->products, function ($products) use ($i, $dataProduct) {
                                    $product = $products->toArray()[$i];
                                    return [
                                        'id' => $product['id'],
                                        'title' => $product['title'],
                                        'slug' => $product['slug'],
                                        'product_sku' => URL::asset(route('api.showcase.products.show', [
                                            'product' => $product['slug'] ?? $product['id']
                                        ])),
                                        'product_price' => $product['price'],
                                        'product_discount' => $dataProduct['discount'],
                                        'product_merchant' => $dataProduct['merchant'],
                                        'product_payment_status' => $dataProduct['payment_status'],
                                        'product_return_order_status' => $dataProduct['product_return_order_status'],
                                        'product_delivery_status' => $dataProduct['parcel_delivery_status'],
                                        'product_status_id' => $product['product_status_id']
                                    ];
                                });
                            });
                        }),
                    ];
                }
            } else {
                $data[] = [
                    'order_id' => $order->id,
                    'parcel_id' => optional(optional($order->parcels)->first())->id,
                    'full_name' => $order->full_name,
                    'locality' => $order->locality,
                    'address' => $order->address,
                    'region' => $order->region,
                    'promo_code' => optional($order->promoCode)->code,
                    'merchant' => optional($order->merchant)->name,
                    'comment' => $order->comment,
                    'phone' => $order->phone,
                    'email' => $order->email,
                    'created_at' => $order->created_at,
                    'updated_at' => $order->updated_at,
                    'delivery' => optional($order->delivery)->name,
                    'delivery_date' => $order->delivery_date,
                    'payment' => optional($order->payment)->name,
                    'parcel_delivery_status' => $parcel_delivery_status,
                    'payment_status' => optional($order->paymentStatus)->name,
                    'amount' => $order->amount,
                    'cost_of_delivery' => $order->cost_of_delivery,
                    'departure_date' => $order->departure_date,
                    'products' => optional($order->parcels, function ($parcels) use ($dataProduct) {
                        return optional($parcels->first(), function ($parcel) use ($dataProduct) {
                            $parcelProducts = $parcel->products;

                            $products = [];

                            $parcelProducts->each(function ($product) use (&$products, $dataProduct) {
                                $products[] = [
                                    'id' => $product['id'],
                                    'title' => $product['title'],
                                    'slug' => $product['slug'],
                                    'product_sku' => URL::asset(route('api.showcase.products.show', [
                                        'product' => $product['slug'] ?? $product['id']
                                    ])),
                                    'product_price' => $product['price'],
                                    'product_discount' => $dataProduct['discount'],
                                    'product_merchant' => $dataProduct['merchant'],
                                    'product_payment_status' => $dataProduct['payment_status'],
                                    'product_return_order_status' => $dataProduct['product_return_order_status'],
                                    'product_delivery_status' => $dataProduct['parcel_delivery_status'],
                                    'product_status_id' => $product['product_status_id']
                                ];
                            });

                            return $products;
                        });
                    })
                ];
            }
        });

        return $data;
    }

    /**
     * @param OrderFilterDto $orderFilterDto
     * @return string
     * @throws NoDataForExportException
     */
    public function orderExport(OrderFilterDto $orderFilterDto)
    {
        $filePath = 'orders/' . $this->generateFileNameForExportOrder();

        $dataOrders = $this
            ->getDataForOrdersExport($orderFilterDto);

        if (count($dataOrders) == 0) {
            throw new NoDataForExportException(__('export.no_data_for_export'));
        }

        Excel::store(
            new OrderExport(
                $dataOrders,
                $orderFilterDto,
                $this->getCheckedFieldsListWithTranslations(),
                $this->getProductFieldsForExport(),
                $this->getCheckedFieldsList(),
            ),
            $filePath,
            'files'
        );

        return URL::asset('storage/files/' . $filePath);
    }

    /**
     * @param $arr
     * @param $arr2
     * @return bool
     */
    public function checkForThePresenceOfAValueInAnArray($arr, $arr2)
    {
        foreach($arr as $value) {
            if (in_array($value, $arr2)) {
                return true;
            }
        }
        return false;
    }

    /**
     * @return string[]
     */
    public function getProductFieldsForExport()
    {
        return $this->productFieldsForExport;
    }

    /**
     * @return string
     */
    public function generateFileNameForExportOrder(): string
    {
        return 'orders_' . Carbon::now()->format('Y-m-d-H-i-s') . '.xlsx';
    }

    /**
     * @return string[]
     */
    public function getCheckedFieldsListWithTranslations() : array
    {
        return $this->checkedFieldsListWithTranslations;
    }

    /**
     * @return string[]
     */
    public function getCheckedFieldsList() : array
    {
        return $this->checkedFieldsList;
    }

    /**
     * @return array
     */
    public function getListOfFilters() : array
    {
        return $this->listFilters;
    }
}
