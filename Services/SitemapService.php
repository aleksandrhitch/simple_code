<?php


namespace App\Services;

use App\Models\Ecommerce\Category;
use App\Models\Ecommerce\Product;
use App\Scopes\WithoutGeneralCategory;
use Carbon\Carbon;
use Spatie\Sitemap\Sitemap;
use Spatie\Sitemap\Tags\Url;

/**
 * Class SitemapService
 * @package App\Services
 */
class SitemapService
{
    protected $sitemap;

    protected $products;

    const HOME_PAGE = '/';

    const PRODUCTS_PRIORITY = 0.2;

    const PRODUCTS_FREQUENCY = 'daily';

    protected $contentPages = [
        'restaurants',
        'contacts',
        'about',
        'about-delivery',
        'public-contract'
    ];

    const CONTENT_PAGES_FREQUENCY = 0.1;

    /**
     * SitemapService constructor.
     */
    public function __construct()
    {
        $this->sitemap = Sitemap::create();
    }

    /**
     * Generate new sitemap.xml file
     */
    public function generate(): void
    {
        $this->sitemap
            ->add(Url::create(self::HOME_PAGE));

        $this->addProducts();

        $this->addContentPages();

        $this->sitemap
            ->writeToFile(public_path(config('sitemap.filename')));
    }

    public function returnProducts()
    {
        $this->products = Product::where('category_id', '<>', Category::withoutGlobalScope(WithoutGeneralCategory::class)
            ->where('removable', 0)->first()->id)
            ->get();
    }

    /**
     * @return mixed
     */
    public function getProducts()
    {
        $this->returnProducts();

        return $this->products;
    }

    /**
     * @param Sitemap $sitemap
     */
    public function addProducts()
    {
        $this->getProducts()
            ->each(function (Product $product) {
                $this
                    ->sitemap
                    ->add(
                        Url::create($product->slug ?? $product->id)
                            ->setLastModificationDate(Carbon::yesterday())
                            ->setPriority(self::PRODUCTS_PRIORITY)
                            ->setChangeFrequency(self::PRODUCTS_FREQUENCY)
                    );
            });
    }

    /**
     * Add content pages
     */
    public function addContentPages()
    {
        $this->getContentPagesCollection()
            ->each(function ($page) {
                $this->sitemap
                    ->add(Url::create($page));
            });
    }

    /**
     * @return \Illuminate\Support\Collection|\Tightenco\Collect\Support\Collection
     */
    public function getContentPagesCollection()
    {
        return collect($this->contentPages);
    }
}
