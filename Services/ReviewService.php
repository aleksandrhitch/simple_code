<?php


namespace App\Services\Showcase;

use App\Dto\Admin\Showcase\Review\ReviewDto;
use App\Dto\Showcase\Review\OrderReviewDto;
use App\Dto\Showcase\Review\ProductReviewDto;
use App\Exceptions\NotAllowedProductReviewCreateException;
use App\Http\Resources\Admin\Showcase\Review\ReviewProductResource;
use App\Models\CRM\Client;
use App\Models\CRM\Order\DeliveryStatus;
use App\Models\CRM\Order\Order;
use App\Models\Showcase\Product;
use App\Models\Showcase\Review;
use App\Models\Showcase\ReviewStatus;
use App\Services\Media\ImageService;
use Illuminate\Support\Facades\URL;

/**
 * Class ReviewService
 * @package App\Services\Showcase
 */
class ReviewService
{
    protected $imageService;

    /**
     * ReviewService constructor.
     * @param ImageService $imageService
     */
    public function __construct(ImageService $imageService)
    {
        $this->imageService = $imageService;
    }

    /**
     * @return mixed
     */
    public function getProductReviewsNewWithPaginate()
    {
        return Review::productNew()
            ->paginate(config('paginate.admin.reviews-products'));
    }

    /**
     * @return mixed
     */
    public function getProductReviewsProcessedWithPaginate()
    {
        return Review::productProcessed()
            ->paginate(config('paginate.admin.reviews-products'));
    }

    /**
     * @param int $id
     * @return ReviewProductResource
     */
    public function getProductReviewResource(int $id)
    {
        $reviewProduct = Review::type('product')
            ->where('id', $id)
            ->firstOrFail();

        return new ReviewProductResource($reviewProduct);
    }

    /**
     * @param $review
     * @param ReviewDto $reviewDto
     * @return ReviewProductResource
     */
    public function update(Review $review, ReviewDto $reviewDto)
    {
        if ($reviewDto->reason_for_rejection) {
            $review->reason_for_rejection = $reviewDto->reason_for_rejection;
        }

        if ($reviewDto->review_status_id) {
            $review->review_status_id = $reviewDto->review_status_id;
        }

        $review->save();

        return new ReviewProductResource($review);
    }

    /**
     * @return mixed
     */
    public function getOrderReviewsWithPagination()
    {
        return Review::type('order')
            ->paginate(config('paginate.admin.reviews-orders'));
    }

    /**
     * @param ProductReviewDto $productReviewDto
     */
    public function createProductReview(ProductReviewDto $productReviewDto)
    {
        $statusData = $this
            ->checkIfThereIsProductInOrders(
                $productReviewDto->product_id,
                auth()->id()
            );

        if ($statusData['status'] == false) {
            throw new NotAllowedProductReviewCreateException(__('products.not_allowed_create_review'));
        }

        if ($statusData['order'] instanceof Order) {
            if (optional($this->getDeliveryStatusProduct($statusData['order']))->id != DeliveryStatus::STATUS_DELIVERED) {
                throw new NotAllowedProductReviewCreateException(__('products.product_not_delivered'));
            }
        }

        if ($statusData['order'] !== false) {
            Review::create(
                array_merge(
                    $productReviewDto->all(),
                    [
                        'product_id' => $productReviewDto->product_id,
                        'client_id' => auth()->id() ?? null,
                        'merchant_id' => $statusData['status'] === true
                            ? optional($statusData['order'])->merchant_id
                            : null,
                        'order_id' => $statusData['order']
                            ? optional($statusData['order'])->id
                            : null,
                        'images' => $this->uploadProductReviewImages($productReviewDto),
                        'reviewable_type' => 'product',
                        'reviewable_id' => $productReviewDto->product_id,
                        'review_status_id' => ReviewStatus::STATUS_NEW,
                        'display_full_name' => $productReviewDto->display_full_name
                            ? Review::DISPLAY_FULL_NAME
                            : Review::HIDDEN_FULL_NAME,
                    ]
                ));
        }
    }

    /**
     * @param ProductReviewDto $productReviewDto
     * @return array
     */
    public function uploadProductReviewImages(ProductReviewDto $productReviewDto)
    {
        $images = [];

        if ($productReviewDto->image_1 != null) {
            $images[] = $this
                ->imageService
                ->save($productReviewDto->image_1, 'files', 'product-reviews');
        }

        if ($productReviewDto->image_2 != null) {
            $images[] = $this
                ->imageService
                ->save($productReviewDto->image_2, 'files', 'product-reviews');
        }

        if ($productReviewDto->image_3 != null) {
            $images[] = $this
                ->imageService
                ->save($productReviewDto->image_3, 'files', 'product-reviews');
        }

        return $images;
    }

    /**
     * @param Order $order
     * @return \Illuminate\Support\Optional|mixed
     */
    public function getDeliveryStatusProduct(Order $order)
    {
        return optional($order->parcels, function ($parcel) {
            return optional($parcel->first(), function ($parcel) {
                return optional($parcel)->deliveryStatus;
            });
        });
    }

    /**
     * @param int $productId
     * @param int $clientId
     * @return array
     */
    public function checkIfThereIsProductInOrders(int $productId, int $clientId)
    {
        $result = [
            'status' => false,
            'order' => false
        ];

        $client = Client::findOrFail($clientId);

        if ($client) {
            $productExists = Product::whereHas('parcels', function ($query) use ($client) {
                return $query->whereHas('order', function ($query) use ($client) {
                    return $query->where('client_id', $client->id);
                });
            })
                ->where('id', $productId)->exists();

            if ($productExists) {
                $result['status'] = true;

                $result['order'] = Order::where('client_id', $client->id)
                    ->whereHas('parcels', function ($query) use ($productId) {
                        return $query->whereHas('products', function ($query) use ($productId) {
                            return $query->where('parcel_product.product_id', $productId);
                        });
                    })->first();
            }
        }

        return $result;
    }

    /**
     * @param OrderReviewDto $orderReviewDto
     */
    public function createOrderReview(OrderReviewDto $orderReviewDto)
    {
        $order = Order::find($orderReviewDto->order_id);

        Review::create(
            array_merge(
                $orderReviewDto->all(),
                [
                    'merchant_id' => $order
                        ? optional($order)->merchant_id
                        : null,
                    'client_id' => auth()->id() ?? null,
                    'reviewable_type' => 'order',
                    'reviewable_id' => $orderReviewDto->order_id,
                    'review_status_id' => ReviewStatus::STATUS_NEW
                ]
            )
        );
    }

    /**
     * @param Client $client
     * @return mixed
     */
    public function getClientReviews(Client $client)
    {
        return Review::with(['productable'])
            ->where('client_id', $client->id)
            ->where('reviewable_type', 'product')
            ->paginate(config('paginate.crm.reviews'));
    }

    /**
     * @param $reviews
     * @return array
     */
    public function getProductReviewsTotalImages($reviews)
    {
        $images = [];

        $reviews
            ->each(function ($review) use (&$images) {
                if (!is_null($review->images)) {
                    if (is_string($review->images)) {
                        $reviewImages = explode(', ', $review->images);

                        if (is_array($reviewImages)) {
                            foreach ($reviewImages as $imagePath) {
                                $images[] = URL::asset('storage/files/'.$imagePath);
                            }
                        }
                    }
                }

                if (is_array($review->images)) {
                    foreach ($review->images as $imagePath) {
                        $images[] = URL::asset('storage/files/'.$imagePath);
                    }
                }
            });

        return $images;
    }

    /**
     * @param $reviews
     * @return mixed
     */
    public function getTotalRating($reviews)
    {
       return round($reviews->avg('rating'));
    }
}
