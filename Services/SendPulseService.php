<?php


namespace App\Services;

use App\Traits\ApiResponseTrait;
use App\Traits\LogsTrait;
use Sendpulse\RestApi\ApiClient;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Str;
use Sendpulse\RestApi\Storage\FileStorage;
use App\Models\Ecommerce\Sms;
use Faker\Factory;

class SendPulseService
{
    use ApiResponseTrait, LogsTrait;

    /**
     * @var ApiClient
     */
    private $client;

    /**
     * @var \Illuminate\Config\Repository|\Illuminate\Contracts\Foundation\Application|mixed
     */
    private $id;

    /**
     * @var \Illuminate\Config\Repository|\Illuminate\Contracts\Foundation\Application|mixed
     */
    private $secret;

    /**
     * SendPulseService constructor.
     * @param $id
     * @param $secret
     */
    public function __construct()
    {
        $this->id = env('SEND_PULSE_ID');
        $this->secret = env('SEND_PULSE_SECRET');
        $this->client = new ApiClient($this->id, $this->secret, new FileStorage());
    }

    /**
     * @param $user
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function sendSms($user)
    {
        $smsCode = $this->generateSmsCode();

        try {
            $result = $this->addAddressBook();
            if (isset($result->error_code)) {
                throw new \Exception('Response from server: ' . $result->message .
                    '. Code: ' . $result->error_code, $result->error_code);
            }
            $bookId = $result->id;
        } catch (\Exception $exception) {
            $this->logException($exception);
            return $this->sendResponse($exception->getMessage(), [], $exception->getCode(), true);
        }

        $params = [
            'sender' => env('SEND_PULSE_SENDER'),
            'body' => __('auth.sms_code_for_enter') . ": " . $smsCode
        ];

        $additionalParams = [
            'transliterate' => 1
        ];

        try {
            $responseAddPhoneNumber = $this->addPhoneNumber($bookId, $user->phone);
            if (isset($responseAddPhoneNumber->is_error)) {
                throw new \Exception('Response from server: ' . $responseAddPhoneNumber->message .
                    '. Code: ' . $responseAddPhoneNumber->error_code, $responseAddPhoneNumber->http_code);
            }
        } catch (\Exception $exception) {
            $this->logException($exception);
            return $this->sendResponse($exception->getMessage(), [], $exception->getCode(), true);
        }

        if ($responseAddPhoneNumber->result == true) {
            try {
                $responseSendSmsByBook = $this
                    ->client
                    ->sendSmsByBook($bookId, $params, $additionalParams);
                if (isset($responseSendSmsByBook->is_error) && $responseSendSmsByBook->is_error == true) {
                    throw new \Exception('Response from server: ' . $responseSendSmsByBook->message .
                        '. Code: ' . $responseSendSmsByBook->error_code, $responseSendSmsByBook->http_code);
                }
                if ($responseSendSmsByBook->result == false) {
                    throw new \Exception('Response from server: ' . json_encode($responseSendSmsByBook), 400);
                }
            } catch (\Exception $exception) {
                $this->logException($exception);
                return $this->sendResponse($exception->getMessage(), [], $exception->getCode(), true);
            }

            $smsEntity = new Sms([
                'code' => $smsCode,
                'phone' => $user->phone
            ]);
            $user->sms()
                ->save($smsEntity);
            return $this->sendResponse(__('auth.code_sent'), [], 200, true);
        }
        return $this->sendResponse(__('auth.error_sending_sms'), [], 400, true);
    }

    /**
     * Get token
     * @return null
     */
    public function getToken()
    {
        $request = Http::post('https://api.sendpulse.com/oauth/access_token', [
            'grant_type' => 'client_credentials',
            'client_id' => $this->id,
            'client_secret' => $this->secret
        ]);
        $response = json_decode("" . $request->body() . "");
        return $response ? $response->access_token : null;
    }

    /**
     * add phone number
     * @throws \Exception
     */
    public function addPhoneNumber($bookId, $phone)
    {
        $data = [
            $phone => [
                [
                    [
                        'name' => 'name',
                        'type' => 'string',
                        'value' => 'variable value',
                    ]
                ]
            ]
        ];
        return $this->client->addPhonesWithVariables($bookId, $data);
    }

    /**
     * add book
     */
    public function addAddressBook()
    {
        $request = Http::withToken($this->getToken())->post('https://api.sendpulse.com/addressbooks', [
            'bookName' => Str::random(10)
        ]);
        return json_decode($request->body());
    }

    /**
     * generate unique code
     * @return int
     */
    public function generateSmsCode()
    {
        $faker = Factory::create();
        return $faker->unique()->randomNumber(6);
    }
}
