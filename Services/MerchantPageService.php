<?php


namespace App\Services\Showcase\Merchant;

use App\Dto\Admin\Showcase\Merchant\MerchantPageDto;
use App\Exceptions\MerchantPageWasFoundException;
use App\Models\Showcase\Merchant\MerchantPage;
use App\Services\Media\ImageService;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Str;

/**
 * Class MerchantPageService
 * @package App\Services\Showcase\Merchant
 */
class MerchantPageService
{
    const MERCHANT_PAGE_ROUTE_SHOW = 'api.showcase.merchant-pages.show';

    protected $imageService;

    /**
     * MerchantPageService constructor.
     * @param $imageService
     */
    public function __construct(ImageService $imageService)
    {
        $this->imageService = $imageService;
    }

    /**
     * @return mixed
     */
    public function getMerchantPages()
    {
        return MerchantPage::paginate(config('paginate.merchant-pages'));
    }

    /**
     * @param int $id
     * @return MerchantPage
     */
    public function getMerchantPage(int $id) : MerchantPage
    {
        return MerchantPage::findOrFail($id);
    }

    /**
     * @param string $slug
     * @return MerchantPage
     */
    public function getMerchantBySlug(string $slug):MerchantPage
    {
        return MerchantPage::where('slug', $slug)
            ->firstOr(function () {
                throw new ModelNotFoundException();
            });
    }

    /**
     * @param MerchantPageDto $merchantPageDto
     * @return mixed
     * @throws MerchantPageWasFoundException
     */
    public function create(MerchantPageDto $merchantPageDto) : MerchantPage
    {
        $merchantPage = $this
            ->findARecordByMerchantId($merchantPageDto->merchant_id);

        if ($merchantPage) {
            throw new MerchantPageWasFoundException();
        }

        $merchantPage = MerchantPage::create(
            array_merge(
                $merchantPageDto->all(),
                [
                    'banner' => $this
                        ->imageService
                        ->save($merchantPageDto->banner, 'files', 'merchant'),
                    'link' => '',
                    'return' => $merchantPageDto->return_policy,
                    'creator_id' => auth()->id()
                ]
            )
        );

        $slug = ($merchantPage->merchant->count() && strlen(optional($merchantPage->merchant)->getTranslation('name', 'ru')) > 0)
            ? Str::slug(optional($merchantPage->merchant)->getTranslation('name', 'ru'))
            : Str::random(10);

        $link = $this->generateUniqueMerchantPageLink($slug);

        $merchantPage->update([
            'link' => $link,
            'slug' => $slug
        ]);

        return $merchantPage;
    }

    /**
     * @param MerchantPage $merchantPage
     * @param MerchantPageDto $merchantPageDto
     * @return MerchantPage
     */
    public function update(MerchantPage $merchantPage, MerchantPageDto $merchantPageDto) : MerchantPage
    {
        if ($merchantPageDto->banner) {
            $this->imageService->delete($merchantPage->banner, 'files');

            $bannerPath = $this
                ->imageService
                ->save($merchantPageDto->banner, 'files', 'merchant');
        }

        $generatedLink = $this->generateMerchantPageLink(
            ($merchantPage->merchant->count() && strlen(optional($merchantPage->merchant)->getTranslation('name', 'ru')) > 0)
                ? Str::slug(optional($merchantPage->merchant)->getTranslation('name', 'ru'))
                : Str::random(10)
        );

        if (isset($bannerPath)) {
            $merchantPage->banner = $bannerPath;
        }

        $merchantPage->return = $merchantPageDto->return_policy;

        $merchantPage->info = $merchantPageDto->info;

        $merchantPage->delivery = $merchantPageDto->delivery;

        $merchantPage->terms_of_sale = $merchantPageDto->terms_of_sale;

        $merchantPage->link = $generatedLink == $merchantPage->link
            ? $merchantPage->link
            : $generatedLink;

        $merchantPage->editor_id = auth()->id();

        $merchantPage->meta_title = $merchantPageDto->meta_title;

        $merchantPage->meta_description = $merchantPageDto->meta_description;

        $merchantPage->meta_keywords = $merchantPageDto->meta_keywords;

        $merchantPage->save();

        return $merchantPage;
    }

    /**
     * @param int $id
     * @return mixed
     */
    public function delete(int $id)
    {
        return MerchantPage::findOrFail($id)
            ->delete();
    }

    /**
     * @param $slug
     * @return string
     */
    public function generateUniqueMerchantPageLink($slug)
    {
        if ($this->checkTheUniquenessOfTheMerchantLink(config('app.url_front').'/merchant/'.$slug)) {
            return config('app.url_front').'/merchant/'.$slug.'-1';
        }
        return config('app.url_front').'/merchant/'.$slug;
    }

    /**
     * @param $slug
     * @return string
     */
    public function generateMerchantPageLink($slug)
    {
        return config('app.url_front').'/merchant/'.$slug;
    }

    /**
     * @param $link
     * @return mixed
     */
    public function checkTheUniquenessOfTheMerchantLink($link)
    {
        return MerchantPage::where('link', $link)->exists();
    }

    /**
     * @param int $id
     * @return mixed
     */
    public function findARecordByMerchantId(int $id)
    {
        return MerchantPage::where('merchant_id', $id)
            ->first();
    }
}
