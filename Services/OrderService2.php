<?php


namespace App\Services;

use App\Http\Requests\ClientOrderStoreFormRequest;
use App\Models\Ecommerce\Cashback;
use App\Models\Ecommerce\Order;
use App\Models\Ecommerce\Product;
use Facades\App\Services\LiqpayService;
use Illuminate\Database\Eloquent\ModelNotFoundException;

/**
 * Class OrderService
 * @package App\Services
 */
class OrderService
{
    protected $totalPrice;

    protected $orderProducts;

    protected $bonusSum;

    protected $bonusProductsSum;

    /**
     * @param ClientOrderStoreFormRequest $request
     */
    public function add($request)
    {
        $data = $request->validated();
        /**
         * status new order
         */
        $data['dct_order_status_id'] = 1;

        /**
         * payment status order
         */
        $data['dct_payment_status_id'] = 1;

        if (auth()->check()) {
            $data['client_id'] = $request->user()->id;
        }

        $this->setOrderProducts($request->items);

        $this->calculateTotalPrice();

        if (auth()->check()) {
            $this->calculateBonusSum($request->user());
            /**
             * pay bonus
             */
            if ($data['dct_payment_id'] == 3) {
                if (!$this->balanceCheck($request->user(), $this->getTotalPrice())) {
                    throw new \Exception(__('users.little_funds_on_the_bonus_account'));
                }
                $this->reduceBalance($request->user(), $this->getTotalPrice());
            } else {
                $this->updateClientBalance($request->user(), $this->getBonusSum());
            }
            $this->updateClientCashbackLevel($request->user());
        }

        $data['total_price'] = $this->getTotalPrice();
        if (isset($data['neighbour'])) {
            $data['neighbour'] = $data['neighbour'] == true ? 1 : false;
        }

        $order = Order::create($data);

        $order->order_info()->create([
            'full_name' => $data['full_name'] ?? null,
            'phone' => $data['phone'] ?? null,
            'restaurant_id' => $data['restaurant_id'] ?? null,
            'table_number' => $data['table_number'] ?? null,
        ]);

        $this->createOrderItems($order, $request->items);

        if ($data['dct_payment_id'] == 1) {
            return LiqpayService::generatePaymentLink($order->id, $this->getTotalPrice());
        }
    }

    /**
     * @return mixed
     */
    public function getBonusSum()
    {
        return $this->bonusSum;
    }

    /**
     * @return mixed
     */
    public function getOrderProducts()
    {
        return $this->orderProducts;
    }

    /**
     * @param $items
     */
    public function setOrderProducts($items)
    {
        $this->orderProducts = Product::whereIn('id', $this->getOrderIdProducts($items))
            ->get();

        if ($this->getOrderProducts()->isEmpty()) {
            throw new ModelNotFoundException(__('products.products_not_found'));
        }

        $newCollection = [];
        foreach ($items as $item) {
            $product = $this
                ->orderProducts
                ->where('id', $item['product_id'])
                ->first();

            if ($product) {
                $product->order_quantity = $item['quantity'];
                $newCollection[] = $product;
            }
        }
        $this->orderProducts = collect($newCollection);
    }

    /**
     * @param $items
     * @return \Illuminate\Support\Collection
     */
    public function getOrderIdProducts($items)
    {
        return collect($items)->pluck('product_id');
    }

    /**
     * @param $order
     * @param $items
     */
    public function createOrderItems($order, $items)
    {
        foreach ($items as $item) {
            $this->createOrderItem($order, $item);
        }
    }

    /**
     * @param $order
     * @param $item
     */
    public function createOrderItem($order, $item)
    {
        $order->items()->create([
            'product_id' => $item['product_id'],
            'sku' => $this->orderProducts->where('id', $item['product_id'])->first()->sku ?? '',
            'name' => $this->orderProducts->where('id', $item['product_id'])->first()->getTranslation('name', 'ru'),
            'quantity' => $item['quantity'],
            'price' => $this->orderProducts->where('id', $item['product_id'])->first()->price
        ]);
    }

    public function calculateTotalPrice()
    {
        $this->orderProducts->each(function ($product) {
            $this->totalPrice += $product->order_quantity * $product->price;
        });
    }

    /**
     * @return mixed
     */
    public function getTotalPrice()
    {
        return $this->totalPrice;
    }

    /**
     * @param $client
     */
    public function updateClientCashbackLevel($client)
    {
        if ($client->cashbackType->name == 'auto') {
            $balanceSum = $this->getBalanceSum($client);
            $cashback = Cashback::whereBetween('minimal_amount', [0, $balanceSum])
                ->orderBy('minimal_amount', 'desc')
                ->first();
            if ($cashback) {
                $client->update([
                    'cashback_id' => $cashback->id
                ]);
            }
        }
    }

    /**
     * ???
     * @param $client
     * @return mixed
     */
    public function getClientCashbackLevel($client)
    {
        return $client->cashback;
    }

    /**
     * @param $client
     * @param $sum
     */
    public function updateClientBalance($client, $sum)
    {
        if ($client->balance()->exists()) {
            return $client
                ->balance
                ->increment('sum', $sum);
        }
        return $client
            ->balance()
            ->create([
                'sum' => $sum,
                'note' => 'Client balance'
            ]);
    }

    /**
     * @param $client
     * @param $orderSum
     */
    public function calculateBonusSum($client)
    {
        $cashback = $this->getClientCashbackLevel($client);

        $this->orderProducts->each(function ($product) use ($client) {
            $productSum = $product->order_quantity * $product->price;
            $this->bonusProductsSum += $productSum  * ($product->cashback_percent / 100);
        });

        $this->bonusSum = $this->bonusProductsSum * ($client->cashback_percent / 100);
        if ($client->cashbackType->name == 'manual' && !is_null($client->cashback_percent) && $client->cashback_percent != 0) {
            return $this->bonusSum = $this->bonusProductsSum * ($client->cashback_percent / 100);
        }
        return $this->bonusSum = $this->bonusProductsSum * ($cashback->percent / 100);
    }

    /**
     * @param $client
     * @return mixed|null
     */
    public function getBalanceSum($client)
    {
        $client->refresh();
        return optional($client->balance)->sum;
    }

    /**
     * @param $client
     * @param $sum
     */
    public function reduceBalance($client, $sum)
    {
        if ($client->balance()->exists() && $client->balance->sum >= $sum) {
            $client->balance()->decrement('sum', $sum);
        }
    }

    /**
     * @param $client
     * @param $sum
     * @return bool
     */
    public function balanceCheck($client, $sum)
    {
        if ($client->balance()->exists() && $client->balance->sum >= $sum) {
            return true;
        }
        return false;
    }

    /**
     * @param $order
     * @param $statusId
     */
    public function updateOrderPaymentStatus($order, $statusId)
    {
        $order->update([
            'dct_payment_status_id' => $statusId
        ]);
    }

    /**
     * @param $request
     * @return float|int
     */
    public function calculateClientCashback($request)
    {
        $this->setOrderProducts($request->validated()['items']);
        $this->calculateTotalPrice();
        return round($this->calculateBonusSum($request->user()));
    }
}
