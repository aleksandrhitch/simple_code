<?php

namespace App\Services\Auth;

use App\Dto\CRM\Auth\ForgotPasswordDto;
use App\Dto\CRM\Auth\LoginByPhoneDto;
use App\Dto\CRM\Client\ClientDto;
use App\Exceptions\AuthFailedException;
use App\Exceptions\Client\BadTokenException;
use App\Http\Resources\CRM\Client\ClientResource;
use App\Models\Admin\User;
use App\Models\CRM\Client;
use App\Models\Message\SmsMessage;
use App\Services\Message\SmsMessageService;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Password;
use Illuminate\Support\Str;
use Illuminate\Testing\Fluent\Concerns\Has;

/**
 * Class AuthorizationService
 * @package App\Services\Auth
 */
class AuthorizationService
{
    /** @var SmsMessageService $smsMessageService */
    protected $smsMessageService;

    /**
     * AuthorizationService constructor.
     * @param SmsMessageService $smsMessageService
     */
    public function __construct(SmsMessageService $smsMessageService)
    {
        $this->smsMessageService = $smsMessageService;
    }

    /**
     * @param Client $user
     * @param LoginByPhoneDto $dto
     * @param bool $isNew
     * @return array|void
     */
    public function loginByPhone(LoginByPhoneDto $dto)
    {
        if (!$dto->code) {
            // no code - send sms
            if($dto->phone !== config('smsc.test_phone')) {
                $code = generate_int_code();
                $this->smsMessageService->sendMessage(
                    $dto->phone,
                    $code,
                    SmsMessage::TYPE_PHONE_VERIFICATION);
            }

            return [
                'isNew' => null,
                'smsSent' => true,
                'code' => "", //$code,
                'token' => null
            ];
        }

        // code received and validated. register user and generate token
        $isNew = false;
        $client = $this->getClientByPhone($dto->phone);
        if (!$client) {
            $isNew = true;
            $client = $this->createClient($dto->all());
        }
        $token = $this->generateToken($client);

        return [
            'isNew' => $isNew,
            'smsSent' => false,
            'code' => "",
            'token' => $token,
            'client' => ClientResource::make($client)
        ];
    }

    /**
     * @param array $data
     * @return Client|\Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Model
     */
    public function createClient($data)
    {
        return Client::query()->create($data);
    }

    /**
     * @param $request
     * @return mixed
     */
    public function getUser($request): User
    {
        return User::where('email', $request->validated()['email'])
            ->firstOr(function () {
                throw new ModelNotFoundException(__('users.not_found'));
            });
    }

    /**
     * @param $email
     * @return mixed
     */
    public function getUserByEmail($email)
    {
        return User::where('email', $email)->first();
    }

    /**
     * @param $user
     * @param $request
     * @return \Illuminate\Http\JsonResponse
     * @throws AuthFailedException
     */
    public function checkPassword($user, $request): void
    {
        if (!$user || !Hash::check($request->validated()['password'], $user->password)) {
            throw new AuthFailedException(__('auth.failed'));
        }
    }

    /**
     * @param $request
     * @return Client
     */
    public function getClientByEmail($request): Client
    {
        return Client::where('email', $request->validated()['email'])
            ->firstOr(function () {
                throw new ModelNotFoundException(__('users.not_found'));
            });
    }

    /**
     * @param $phone
     * @return null|Client
     */
    public function getClientByPhone($phone)
    {
        return Client::where('phone', $phone)
            ->first();
    }

    /**
     * @param Request $request
     * @return bool
     */
    public function revokeToken(Request $request): bool
    {
        if ($request->user()->token()->revoke()) {
            return true;
        }
        return false;
    }

    /**
     * @param $client
     * @return string
     */
    public function generateToken($client): string
    {
        return $client->createToken(Str::random(15))
            ->accessToken;
    }

    /**
     * @param $request
     * @return string
     */
    public function sendResetLink($request): string
    {
        return Password::sendResetLink(
            $request->only('email')
        );
    }

    /**
     * @param $request
     * @return bool
     */
    public function resetPassword($request): bool
    {
        if (Password::reset(
            $request->only('email', 'password', 'password_confirmation', 'token'),
            function ($user, $password) use ($request) {
                $user->forceFill([
                    'password' => Hash::make($password)
                ]);
                $user->save();
            }
        )) {
            return true;
        }
        return false;
    }

    /**
     * @param ClientDto $clientDto
     * @return mixed
     * @throws \Exception
     */
    public function clientResetPassword(ClientDto $clientDto)
    {
        if (!$this->checkRelevanceToken($clientDto->email, $clientDto->token)) {
            throw new BadTokenException(__('auth.token_expired'));
        }

        return $this->clientUpdatePassword($clientDto->email, $clientDto->password);
    }

    /**
     * @param $email
     * @param $password
     * @return mixed
     */
    public function clientUpdatePassword($email, $password)
    {
        $client = Client::where('email', $email)
            ->firstOrFail();

        $client->password = Hash::make($password);

        return $client->save();
    }

    /**
     * @param $mail
     * @param $token
     * @return bool
     */
    public function checkRelevanceToken($mail, $token)
    {
        $passwordResetEntity = DB::table('password_resets')
            ->where('email', $mail)
            ->firstOrFail();

        return Hash::check($token, $passwordResetEntity->token);
    }

    /**
     * @param User $user
     * @return bool
     */
    public function userDeactivated(User $user)
    {
        return $user->is_active == 0
            || $user->is_active == ''
            || $user->is_active == false;
    }

    /**
     * @param ForgotPasswordDto $forgotPasswordDto
     */
    public function clientSendResetPasswordLink(ForgotPasswordDto $forgotPasswordDto)
    {
        $client = Client::whereEmail($forgotPasswordDto->email)
            ->firstOrFail();

        $token = app('auth.password.broker')
            ->createToken($client);

        $client->sendPasswordResetNotification($token);
    }
}
